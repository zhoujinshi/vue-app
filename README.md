#vue-app

## 安装依赖

```
git clone https://git.oschina.net/chenjianlong/vue-app.git
npm install
```

## 运行开发环境

```bash
git checkout develop
npm run dev
```

## 运行生产环境

```bash
git checkout master
npm run build
npm run pro
```
